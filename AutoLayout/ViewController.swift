//
//  ViewController.swift
//  AutoLayout
//
//  Created by User on 2019/7/10.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let bearImageView : UIImageView = {
       let iv = UIImageView(image: #imageLiteral(resourceName: "bear_first"))
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    let descriptionTextView: UITextView = {
       let textView = UITextView ()
        
        let attributeText = NSMutableAttributedString(string: "Join us today in our fun and gemes !", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)])
        attributeText.append(NSAttributedString(string: "\n\nAre you ready for loads and loads fun? Don't wait any longer! we hope to see you in our store soon", attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 18), NSAttributedString.Key.foregroundColor:UIColor.gray]))
        textView.attributedText = attributeText
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.textAlignment = .center
        textView.isEditable = false
        
        return textView
    }()
    
    let prevButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitleColor(.gray, for: .normal)
        btn.setTitle("PREV", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        return btn
    }()
    let nextButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitleColor(.red, for: .normal)
        btn.setTitle("NEXT", for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        return btn
    }()
    let pageControl: UIPageControl = {
       let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = 4
        pc.currentPageIndicatorTintColor = .red
        pc.pageIndicatorTintColor = .gray
        return pc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupLayout()
        setupButtonControls()
    }
    fileprivate func setupButtonControls(){
        let yellowView = UIView()
        yellowView.backgroundColor = .yellow
        
        let redView = UIView()
        redView.backgroundColor = .red
        
        let buttomControlStackView = UIStackView(arrangedSubviews: [prevButton,pageControl,nextButton])
        buttomControlStackView.translatesAutoresizingMaskIntoConstraints = false
        buttomControlStackView.distribution = .fillEqually
        
        view.addSubview(buttomControlStackView)
        NSLayoutConstraint.activate([buttomControlStackView.bottomAnchor.constraint(equalTo: view.bottomAnchor), buttomControlStackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor), buttomControlStackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor), buttomControlStackView.heightAnchor.constraint(equalToConstant: 50)])
        
    }
    fileprivate func setupLayout () {
        let topImageContainer = UIView()
        view.addSubview(topImageContainer)
        topImageContainer.translatesAutoresizingMaskIntoConstraints = false
        
        topImageContainer.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topImageContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topImageContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topImageContainer.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        
        topImageContainer.addSubview(bearImageView)
        bearImageView.centerXAnchor.constraint(equalTo: topImageContainer.centerXAnchor).isActive = true
        bearImageView.centerYAnchor.constraint(equalTo: topImageContainer.centerYAnchor).isActive = true
        bearImageView.heightAnchor.constraint(equalTo: topImageContainer.heightAnchor, multiplier: 0.5).isActive = true

         view.addSubview(descriptionTextView)
        descriptionTextView.topAnchor.constraint(equalTo: topImageContainer.bottomAnchor).isActive = true
        descriptionTextView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        descriptionTextView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        descriptionTextView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }


}

